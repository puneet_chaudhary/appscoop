import React, { Component } from "react";
import "./scss/list.scss";

export default class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bookdetail: {}
    };
  }

  componentDidMount() {
    this.setState({ bookdetail: this.props.location.state.item }, () => {});
  }

  render() {
    let { bookdetail } = this.state;

    return (
      <div className="top">
        {Object.keys(bookdetail).length > 0 && (
          <div className="outer-side">
            <img src={bookdetail.volumeInfo.imageLinks.thumbnail} />
            <div className="desc">Author- {bookdetail.volumeInfo.authors}</div>
            <div className="desc">
              Published Date- {bookdetail.volumeInfo.publishedDate}
            </div>
          </div>
        )}
      </div>
    );
  }
}
