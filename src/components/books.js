import React, { Component } from 'react';
import axios from "axios";
import "./scss/books.scss";
import { Link } from "react-router-dom";

export default class Books extends Component {
    constructor(props) {
        super(props);
        this.state = {
          books: []
        };
      }

      getApiData = () => {
        axios
          .get(
            "https://www.googleapis.com/books/v1/volumes?filter=free-ebooks&q=a"
          )
          .then(res => {
            if (res) {
                
             this.setState({books:res.data.items},()=>{
               
             })
            }
          })
          .catch(err => {
            console.log("err", err);
          });
        
      };

    
      componentDidMount() {
        this.getApiData();
       
      }
    render() {
        let {books} = this.state
        return (
            <div className="top">
                  <div className="heading">We Have Number of Books</div>
             {books &&
              books.map((item, index) => {
                return (
                  <Link to={{pathname:`/book-detail/${index+1}`, state:{item}}}><div className="outer-side">
                   
                  
                    <div className="number">{index+1})</div>
                    <div className="desc">Title-{" "}{item.volumeInfo.title}</div>
                    <div className="desc">SubTitle-{" "}{item.volumeInfo.subtitle}</div>
                  </div></Link>
                );
              })} 
          </div>
        );
    }
}

