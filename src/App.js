import React from "react";
import { BrowserRouter, Switch, Route, Redirect, Link } from "react-router-dom";
import universal from "react-universal-component";

const list = universal(props => import("./components/list"));
const books = universal(props => import("./components/books"));

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={books} />
          <Route exact path="/book-detail/:id" component={list} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
